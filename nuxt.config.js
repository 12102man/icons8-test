module.exports = {
    head: {
        title: 'Instant Icons',
        meta: [
            {hid: 'description', name: 'description', content: 'img-omg - super cool instant icons'}
        ],
    },
    css: [
        'assets/core.scss',
        'assets/fonts/stylesheet.css',
    ],
}
