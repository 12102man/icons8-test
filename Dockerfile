FROM node:latest

RUN mkdir -p /usr/src/icons8-test
WORKDIR /usr/src/icons8-test

COPY . .
RUN npm install
RUN npm run build

EXPOSE 5002

ENV NUXT_HOST=0.0.0.0
# set app port
ENV NUXT_PORT=5004
