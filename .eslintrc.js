module.exports = {
  extends: [
    "plugin:vue/essential",
    "plugin:vue/base",
    "plugin:vue/strongly-recommended",
  ],
  parserOptions: {
    "vue/component-name-in-template-casing": ["error", "PascalCase" | "kebab-case", {
      "registeredComponentsOnly": true,
      "ignores": []
    }]
  }
}
