// Source: https://gist.github.com/weber/5fbfcc245b811191119be3b239623315

export default {
  // Включает абстрактный компонент во Vue.
  // Это свойство не задокументировано и может измениться в любой момент,
  // но ваш компонент должен работать без него.
  abstract: true,

  // Входные параметры отлично работают в абстрактных компонентах!
  props: {
    threshold: {
      type: Array
    }
  },

  // Ура, функции отрисовки!
  render() {
    // Без компонента-обёртки, мы можем отрисовать только один дочерний компонент.
    try {
      return this.$slots.default[0];
    } catch (e) {
      throw new Error('IntersectionObserver.vue может отрисовывать один и только один дочерний компонент.');
    }
  },

  mounted () {
    // Нет реальной потребности объявлять наблюдателя в качестве свойства данных,
    // потому что он не должен быть реактивным.

    this.observer = new IntersectionObserver((entries) => {
      this.$emit(entries[0].isIntersecting ? 'intersect-enter' : 'intersect-leave', [entries[0]]);
    }, {
      threshold: this.threshold || 0
    });

    // Нужно подождать следующего тика для того, чтобы дочерний элемент мог отрисоваться.
    this.$nextTick(() => {
      this.observer.observe(this.$slots.default[0].elm);
    });
  },

  destroyed() {
    // Кстати, почему W3C выбрало "disconnect" для названия метода?
    this.observer.disconnect();
  }
}
