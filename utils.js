/**
 * Generates icon sources for modal
 * according to retrieve format
 *
 * 2 options can be applied here:
 * 1 - use ID for correct retrieval
 *    https://img.icons8.com/?id=1234
 * 2 - concatenate common name and platform:
 *    https://img.icons8.com/{platform}/{commonName}
 *
 * @param {icon} icon object from API
 * @returns {{defaultSrc: string, styleSrc: string, hexColorSrc: string, sizeSrc: string}} set of URLs for modal and for icon rendering in search
 */

const generateIconSrc = ({ id, platform, commonName }) => {
  const isRetrievedByCommonName = platform && commonName;
  const highlightText = (text) => { return `<span class="cb--violet">${text}</span>`; }
  if (isRetrievedByCommonName) {
    return {
      defaultSrc: `https://img.icons8.com/${ platform }/${ commonName }`,
      styleSrc: `https://img.icons8.com/${ highlightText(platform) }/${ commonName }`,
      hexColorSrc: `https://img.icons8.com/${ highlightText('000000') }/${ platform }/${ commonName }`,
      sizeSrc: `https://img.icons8.com/${ platform }/${ commonName }/${ highlightText('50')}`,
      srcSchema: `https://img.icons8.com/${ highlightText('color') }/${ highlightText('style') }/${ highlightText('name') }/${ highlightText('size')}`,
    }
  } else {
    return {
      defaultSrc: `https://img.icons8.com/?id=${ id }`,
      styleSrc: `https://img.icons8.com/?id=${ id }&${ highlightText(`style=ios`)}`,
      hexColorSrc: `https://img.icons8.com/?id=${ id }&${ highlightText(`color=#000000`)}`,
      sizeSrc: `https://img.icons8.com/?id=${ id }&${ highlightText(`size=50`) }`,
      srcSchema: `https://img.icons8.com/?id=${ highlightText(`id`) }&style=${ highlightText(`style`) }&color=${ highlightText(`color`) }&size=${ highlightText(`size`) }`,
    }
  }
};

export default { generateIconSrc }
