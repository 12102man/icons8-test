import axios from 'axios'

export const searchApi = axios.create({
  baseURL: 'https://search.icons8.com/',
  timeout: 5000
});

export const iconsApi = axios.create({
  baseURL: 'https://api-icons.icons8.com/',
  timeout: 5000
});
