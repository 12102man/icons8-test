import { searchApi, iconsApi } from "./index";

export const findIcon = (term) => {
  return searchApi.get('api/iconsets/v5/search', {
    params: {
      term,
      amount: 5
    }
  });
}

export const getIconByID = (id) => {
  return iconsApi.get('publicApi/icons/icon', {
    params: { id }
  });
}
